<?php

namespace Drupal\amswap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AmswapConfigForm.
 *
 * @package Drupal\amswap\Form
 */
class AmswapConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'amswap.amswapconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amswap_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amswap.amswapconfig');
    $form['role_menu_pairs'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Role &amp; menu pairs'),
      '#description' => $this->t('Roles and their associated menus. Eg &quot;owner:owner-menu; ...&quot;'),
      '#default_value' => $config->get('role_menu_pairs'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('amswap.amswapconfig')
      ->set('role_menu_pairs', $form_state->getValue('role_menu_pairs'))
      ->save();
  }

}
